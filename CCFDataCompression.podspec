Pod::Spec.new do |spec|
	spec.name					= 'CCFDataCompression'
	spec.version				= '1.0.10'
	spec.summary 				= 'NSData categories for data compression.'
	spec.homepage 				= 'https://bitbucket.org/nsbum/ccfdatacompression'
	spec.author 				= { "Alan Duncan" => "alan@cocoa-factory.com" }
	spec.source 				= { :git => 'https://bitbucket.org/nsbum/ccfdatacompression/src', :tag => '1.0.10'}
	spec.source_files			= 'core/**/*.{h,m}'
	spec.license 				= 'MIT'
	spec.requires_arc 			= true
	spec.ios.deployment_target 	= '5.0'
	spec.osx.deployment_target 	= '10.6'
	spec.frameworks				= 'libz'
end
